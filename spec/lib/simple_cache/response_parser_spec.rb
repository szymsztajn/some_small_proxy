require_relative '../../spec_helper'

describe SimpleCache::ResponseParser do

  let(:data) {
    ["HTTP/1.1 200 OK",
     "Content-Type: text/html;charset=utf-8",
     "Cache-Control: max-age=10",
     "Content-Length: 52",
     "Connection: keep-alive",
     "Server: thin 1.6.1 codename Death Proof",
     "\r\n",
     "google request received at 2014-02-01 22:59:00 +0100"].
     join("\r\n")
  }

  let(:headers) {
    {
      "Content-Type" => "text/html;charset=utf-8",
      "Cache-Control" => "max-age=10",
      "Content-Length" => "52",
      "Connection" => "keep-alive",
      "Server" => "thin 1.6.1 codename Death Proof"
    }
  }

  let(:server) { double }
  let(:parser) { SimpleCache::ResponseParser.new(server) }

  describe "#response_complete_callback" do

    it "informs the server to update the cache when response is complete" do
      allow(server).to receive(:insert_to_cache)
      parser.add_chunk(data)

      expect(server).to have_received(:insert_to_cache).
                        with(headers, data)
    end

  end


end