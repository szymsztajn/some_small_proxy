require_relative '../../spec_helper'

describe SimpleCache::RequestParser do

  let(:data) {
    ["GET /new_request HTTP/1.1",
     "Host: localhost:9292",
     "Connection: close",
     "\r\n"].
     join("\r\n")
  }

  let(:server) { double }
  let(:parser) { SimpleCache::RequestParser.new(server) }


  describe "#request_complete_callback" do

    it "informs the server when request is complete" do
      allow(server).to receive(:respond_or_propagate)
      parser.add_chunk(data)

      expect(server).to have_received(:respond_or_propagate).
                        with("/new_request", data)
    end

    it "sets path to nil for non-GET request" do
      post_data = data.gsub("GET", "POST")
      allow(server).to receive(:respond_or_propagate)

      parser.add_chunk(post_data)
      expect(server).to have_received(:respond_or_propagate).
                        with(nil, post_data)
    end

  end


end