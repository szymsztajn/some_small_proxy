require_relative './../../../spec_helper'

describe SimpleCache::Adapters::Memory do

  let(:store) { SimpleCache::Adapters::Memory.new }

  describe "#insert" do

    it "stores data" do
      allow(Time).to receive(:now).and_return(Time.now)

      store.insert("test_key1", 100, "test_data1")
      expect(store.retrieve("test_key1")).to eq("test_data1")
    end

  end

  describe "#retrieve" do

    it "retrieves previously stored data" do
      allow(Time).to receive(:now).and_return(Time.now)
      store.insert("test_key2", 100, "test_data2")

      expect(store.retrieve("test_key2")).to eq("test_data2")
    end

    it "doesn't retrieve data which ttl has passed" do
      store.insert("testk_key3", 10, "test_data3")
      allow(Time).to receive(:now).and_return(Time.now + 200)

      expect(store.retrieve("test_key3")).to be_nil
    end

    it "returns nil for nonexistent key" do
      expect(store.retrieve("no key")).to be_nil
    end

  end

  describe "#ttl" do

    it "returns ttl based on current time" do
      allow(Time).to receive(:now).and_return(Time.now)
      store.insert("test_key4", 100, "test_data")

      expect(store.ttl("test_key4")).to eq(100)
    end

    it "returns nil for nonexistent key" do
      expect(store.ttl("no key")).to be_nil
    end

  end

end