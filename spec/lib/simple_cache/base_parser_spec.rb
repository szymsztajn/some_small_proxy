require_relative '../../spec_helper'

describe SimpleCache::BaseParser do

  describe "#add_chunk" do

    it "adds data to Http::Parser" do
      parser = double
      allow(parser).to receive(:<<)

      allow(Http::Parser).to receive(:new).and_return(parser)
      baser_parser = SimpleCache::BaseParser.new(double)
      baser_parser.add_chunk("data")

      expect(parser).to have_received(:<<).with("data")
    end

  end

end