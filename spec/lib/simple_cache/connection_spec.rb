require_relative '../../spec_helper'

describe SimpleCache::Connection do

  let(:server) { double }
  let(:data) { double }

  before { @connection = SimpleCache::Connection.new(nil, server) }

  describe "#receive_data" do

    it "sends received data to server" do
      allow(server).to receive(:return_data)
      @connection.receive_data(data)
      expect(server).to have_received(:return_data).with(data)

    end

  end

  describe "#unbind" do
    it "unbinds server" do
      allow(server).to receive(:unbind_connection)
      @connection.unbind
      expect(server).to have_received(:unbind_connection)
    end
  end

end