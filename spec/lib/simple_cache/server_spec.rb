require_relative '../../spec_helper'

describe SimpleCache::Server do

  let(:connection) { double }
  let(:request_parser) { double }
  let(:response_parser) { double }
  let(:storage) { double }
  let(:data) { double }

  before do
    allow(EventMachine).to receive(:connect).and_return(connection)
    allow(SimpleCache::RequestParser).to receive(:new).and_return(request_parser)
    allow(SimpleCache::ResponseParser).to receive(:new).and_return(response_parser)

    @server = SimpleCache::Server.new(nil, storage: storage, dest_host: "0.0.0.1",
                                      dest_port: "81")
  end

  describe ".new" do

    it "opens a connection with specified parameters" do
      stub_const("EventMachine", EventMachine)
      allow(EventMachine).to receive(:connection)

      s = SimpleCache::Server.new(nil, storage: storage, dest_host: "127.0.0.1",
                                  dest_port: "8080")
      expect(EventMachine).to have_received(:connect).with(
                              "127.0.0.1", "8080", SimpleCache::Connection, s)
    end

  end

  describe "#receive_data" do

    it "passes the data to request parser" do
      allow(request_parser).to receive(:add_chunk)

      @server.receive_data(data)
      expect(request_parser).to have_received(:add_chunk).with(data)
    end

  end

  describe "#return data" do

    it "passes the data to response parser" do
      allow(@server).to receive(:send_data)
      allow(response_parser).to receive(:add_chunk)

      @server.return_data(data)
      expect(response_parser).to have_received(:add_chunk).with(data)
      expect(@server).to have_received(:send_data).with(data)
    end

  end

  describe "#unbind" do

    it "sets callback to close connection after writing" do
      allow(connection).to receive(:close_connection_after_writing)
      @server.unbind

      expect(connection).to have_received(:close_connection_after_writing).once
    end

  end

  describe "#unbind_connection" do

    it "unbinds itself" do
      allow(@server).to receive(:close_connection_after_writing)
      @server.unbind_connection

      expect(@server).to have_received(:close_connection_after_writing).once
    end

  end

  describe "#insert into cache" do

    it "passes data into storage" do
      allow(storage).to receive(:insert)
      @server.insert_to_cache({ "header" => "value" }, "data")

      expect(storage).to have_received(:insert).with(nil, { "header" => "value" }, "data")
    end

  end

  describe "#respond_or_propagate" do

    it "responds with data from cache, if it's found" do
      allow(storage).to receive(:retrieve).and_return(data)
      allow(connection).to receive(:close_connection)
      allow(@server).to receive(:send_data)

      @server.respond_or_propagate("/test_path", double)
      expect(@server).to have_received(:send_data).with(data)
      expect(connection).to have_received(:close_connection)
    end

    it "sends request to server, if reponse not in cache" do
      allow(storage).to receive(:retrieve).and_return(nil)
      allow(connection).to receive(:send_data)

      @server.respond_or_propagate("/test_path", data)
      expect(connection).to have_received(:send_data).with(data)
    end

  end

end