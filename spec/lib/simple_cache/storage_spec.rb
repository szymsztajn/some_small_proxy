require_relative '../../spec_helper'

describe SimpleCache::Storage do

  describe "#insert" do

    let(:headers) {
                    {
                      "Content-Type"=>"text/html;charset=utf-8",
                      "Cache-Control"=>"max-age=10",
                      "Content-Length"=>"52",
                      "X-XSS-Protection"=>"1; mode=block",
                      "X-Content-Type-Options"=>"nosniff",
                      "X-Frame-Options"=>"SAMEORIGIN",
                      "Connection"=>"keep-alive",
                      "Server"=>"thin 1.6.1 codename Death Proof"
                    }
                   }

    it "inserts data to storage with correct attributes" do
      adapter = double
      allow(adapter).to receive(:insert)
      storage = SimpleCache::Storage.new(adapter)

      storage.insert("/some_url", headers, "Example")
      expect(adapter).to have_received(:insert).with("/some_url", 10, "Example")
    end

    it "doesn't insert the data if the path is nil" do
      adapter = double()
      allow(adapter).to receive(:insert)
      storage = SimpleCache::Storage.new(adapter)

      storage.insert(nil, headers, "Example")
      expect(adapter).to_not have_received(:insert)
    end

  end

  describe "#retrieve" do

    let(:data) { ["HTTP/1.1 200 OK", "Cache-Control: max-age=10", "\r\n",
                  "google request received at 2014-02-01 22:59:00 +0100"].join("\r\n") }

    it "adapts response ttl to current ttl of the request" do
      adapter = double(:retrieve => data, :ttl => 7)
      storage = SimpleCache::Storage.new(adapter)

      expect(storage.retrieve("/test")).to eq(
                        ["HTTP/1.1 200 OK", "Cache-Control: max-age=7", "\r\n",
                         "google request received at 2014-02-01 22:59:00 +0100"].join("\r\n"))
    end

    it "returns nil for no path" do
      storage = SimpleCache::Storage.new(double)
      expect(storage.retrieve(nil)).to be_nil
    end

    it "returns nil for paths not found in cache" do
      adapter = double(:retrieve => nil)
      storage = SimpleCache::Storage.new(adapter)

      expect(storage.retrieve("/test")).to be_nil
    end
  end

end