#!/usr/bin/env ruby
require 'sinatra'
require 'httparty'

class App < Sinatra::Base

  get "/google_request" do
    ttl = params["ttl"] || 10
    response.headers["Cache-Control"] = "max-age=#{ttl}"
    HTTParty.get('https://www.google.pl')
    "google request received at #{Time.now.to_s}"
  end

  post "/post_me" do
    "Posted on #{Time.now}"
  end

end