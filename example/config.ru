#!/usr/bin/env ruby
lib = File.expand_path(File.dirname(__FILE__))
$:.unshift(lib)

require 'app'

run App