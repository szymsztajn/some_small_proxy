centralway_proxy
================

HTTP Proxy built using Event Machine. Procfile includes commands to start both proxy and example sinatra app.

What needs improvement:

* Setup. Host and port should be set via shell or config, now they are hardcoded in bin/simple_proxy

* HTTP standard compliance. `Cache-control: max-age` is only supported when sent in response, but not in request

* Consider changing to threads(EM is not believed to be the best solution for modern Ruby apps)

* Socket support. Should be very easy to implement, thanks to EM

* Keep-alive errors. When testing with JMeter 40% of responses were corrupted.
