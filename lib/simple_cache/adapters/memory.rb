module SimpleCache
  module Adapters
    class Memory

      def initialize
        @data_storage = {}
        @expire_storage = {}
      end

      def insert(key, ttl, data)
        @data_storage[key] = data
        @expire_storage[key] = Time.now + ttl
      end

      def retrieve(key)
        if @expire_storage[key] && @expire_storage[key] >= Time.now
          @data_storage[key]
        end
      end

      def ttl(key)
        if (expires = @expire_storage[key])
          (expires - Time.now).to_i
        end
      end
    end
  end
end