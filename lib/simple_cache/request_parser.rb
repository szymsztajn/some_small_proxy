module SimpleCache
  class RequestParser < SimpleCache::BaseParser

    def initialize(proxy_server)
      super
      @parser.on_message_complete = ->() { request_complete_callback }
    end

    def request_complete_callback
      path = (@parser.http_method == "GET" ? @parser.request_url : nil)
      @proxy_server.respond_or_propagate(path, @data)
      @data = ""
    end

  end
end