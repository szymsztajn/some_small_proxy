module SimpleCache
  class ResponseParser < SimpleCache::BaseParser

    def initialize(proxy_server)
      super
      @parser.on_message_complete = ->() { response_complete_callback }
    end

    def response_complete_callback
      @proxy_server.insert_to_cache(@parser.headers, @data)
      @data = ""
    end

  end
end