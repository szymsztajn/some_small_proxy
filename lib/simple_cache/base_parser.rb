require 'http/parser'

module SimpleCache
  class BaseParser

    def initialize(proxy_server)
      @proxy_server = proxy_server
      @parser = Http::Parser.new
      @data = ""
    end

    def add_chunk(chunk)
      @data << chunk
      @parser << chunk
    end

  end
end