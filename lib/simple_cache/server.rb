require 'eventmachine'

module SimpleCache
  class Server < EM::Connection

    def initialize(opts) #opts = { storage: , dest_host: , dest_port: }
      @connection = EventMachine.connect(opts[:dest_host], opts[:dest_port],
                                         SimpleCache::Connection, self)

      @request_parser = SimpleCache::RequestParser.new(self)
      @response_parser = SimpleCache::ResponseParser.new(self)
      @storage = opts[:storage]
    end

    # EventMachine callbacks

    def receive_data(data)
      @request_parser.add_chunk(data)
    end

    def return_data(data)
      @response_parser.add_chunk(data)
      send_data(data)
    end

    def unbind
      @connection.close_connection_after_writing
    end

    def unbind_connection
      close_connection_after_writing
    end

    # data flow control(invoked by http parsers)

    def insert_to_cache(headers, data)
      @storage.insert(@path, headers, data)
    end

    def respond_or_propagate(path, data)
      @path = path

      if res = @storage.retrieve(@path)
        send_data(res)
        @connection.close_connection
      else
        @connection.send_data(data)
      end
    end

  end
end