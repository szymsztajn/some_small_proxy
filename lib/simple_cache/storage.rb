module SimpleCache
  class Storage

    def initialize(adapter)
      @adapter = adapter
    end

    def insert(path, headers, data)
      if path
        ttl = (headers["Cache-Control"] && headers["Cache-Control"].scan(/(?<=max-age=)\d+/).first).to_i
        @adapter.insert(path, ttl, data)
      end
    end

    def retrieve(path)
      if path && (response = @adapter.retrieve(path))
        ttl = @adapter.ttl(path)
        response.gsub(/Cache-Control: max-age=\d+/, "Cache-Control: max-age=#{ttl}")
      end
    end

  end
end