require 'eventmachine'

module SimpleCache
  class Connection < EM::Connection

    def initialize(server)
      @server = server
      super
    end

    def receive_data(data)
      @server.return_data(data)
    end

    def unbind
      @server.unbind_connection
    end

  end
end