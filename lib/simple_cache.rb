Dir.glob(File.dirname(__FILE__) + "/../lib/simple_cache/adapters/**/*.rb").each { |f| require f }

$:.unshift(File.dirname(__FILE__) + "/../lib/simple_cache")

require 'base_parser'
require 'request_parser'
require 'response_parser'
require 'storage'
require 'connection'
require 'server'